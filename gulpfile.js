var gulp = require('gulp'),
    sass = require('gulp-sass'),
    pug = require('gulp-pug'),
    clean = require('gulp-clean-css'),
    concat = require('gulp-concat'),
    connect = require('gulp-connect'),
    browserify = require('browserify'),
    babelify = require('babelify'),
    pugify = require('pugify'),
    source = require('vinyl-source-stream');

gulp.task('pug', function() {
  return gulp.src(['src/app/**/*.pug', '!src/index.pug'])
    .pipe(pug().on('error', err => {
      console.log(err);
    }))
    .pipe(gulp.dest('static/tmp'))
    .pipe(connect.reload());
});

gulp.task('index', function() {
  return gulp.src('src/index.pug')
    .pipe(pug().on('error', err => {
      console.log(err);
    }))
    .pipe(gulp.dest('static'))
    .pipe(connect.reload());
});

gulp.task('sass', function() {
  return gulp.src(['src/assets/**/*.sass', 'src/app/**/*.sass'])
    .pipe(sass({
      includePath: './'
    }).on('error', err => {
      console.log(err);
    }))
    .pipe(concat('style.css'))
    .pipe(clean())
    .pipe(gulp.dest('static/css'))
    .pipe(connect.reload());
});

gulp.task('js', function() {
  browserify({
    entries: 'src/app/app.js'
  }).on('error', err => {
      console.log(err);
    })
  .transform(pugify.pug({pretty: false}))
  .transform(babelify.configure({
    presets: 'es2015'
  })).on('error', err => {
    console.log(err);
  })
  .bundle()
  .pipe(source('bundle.js'))
  .pipe(gulp.dest('static/js'))
  .pipe(connect.reload());
});

gulp.task('connect', function() {
  connect.server({
    root: 'static',
    fallback: './static/index.html',
    port: '1337',
    livereload: true
  });
});

gulp.task('font', function() {
  return gulp.src([
    'node_modules/font-awesome/fonts/*.{ttf,otf,eot,svg,woff,woff2}',
    'node_modules/bootstrap/fonts/*.{ttf,otf,eot,svg,woff,woff2}'
    ])
    .pipe(gulp.dest('static/fonts'));
});
    
gulp.task('watch', function() {
  gulp.watch(['src/app/**/*.pug'], ['js', 'pug']);
  gulp.watch(['src/index.pug'], ['index']);
  gulp.watch(['src/assets/**/*.sass', 'src/app/**/*.sass'], ['sass']);
  gulp.watch(['src/app/**/*.js'], ['js']);
});

gulp.task('build', ['pug', 'index', 'sass', 'js', 'font'], function(done) {
  console.log('project build');
  done();
});

gulp.task('default', ['build', 'connect', 'watch']);
