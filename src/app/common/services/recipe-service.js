'use strict'

function recipeService(
  $state,
  $rootScope,
  apiService,
  $q
  ) {

  this.getById = function(id) {
    return apiService.call('GET', `recipe/${id}`)
  }

  this.getByIds = function(arr) {
    return apiService.call('POST', 'recipe', arr);
  }

  this.get = function() {
    return apiService.call('GET', 'recipe/list');
  }

}

module.exports = angular
  .module('common')
  .service('recipeService', recipeService)
  .name;