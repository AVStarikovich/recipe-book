'use strict'

const appConfig = require('../../config');

function userService(
  apiService,
  $http,
  $q
) {

  this._users = [];

  this.signIn = function(login, password, email) {
    return apiService.call('POST', 'user/register', {login, password, email} );
  }

  this.login = function(login, password) {
    let defer = $q.defer();
    
    $http.get(`${appConfig.api}/user/login`, {
      headers: {
        'Authorization': 'Basic ' + window.btoa(`${login}:${password}`)
      }
    }).then(res => {
      if (res.data.success) {
        apiService.setToken(res.data.data.accessToken);
        defer.resolve(res.data.data);
      } else defer.reject(res.data.error);
    }).catch(rej => {
      defer.reject(rej);
    });

    return defer.promise;

  };

  this.loginByToken = function(token) {
    apiService.setToken(token);
    return apiService.call('GET', 'user/by-token');
  };

  this.getUserByLogin = function(login) {
    let defer = $q.defer();
    if (this._users.length > 30) {
      this._users = [];
    }
    if (this._users.length > 0) {
      _.forEach(this._users, function(value){
        if (value.login == login) {
          defer.resolve(value);
        } else {
          apiService.call('GET', `user/by-login/${login}`)
            .then(data => {
              this._users.push(data);
              defer.resolve(data);
            })
            .catch(data => {
              defer.resolve(data);
            });
        };
      });
    } else {
      apiService.call('GET', `user/by-login/${login}`)
        .then(data => {
          this._users.push(data);
          defer.resolve(data);
        })
        .catch(data => {
          defer.resolve(data);
        });
        
     }
     return defer.promise;
  };

  this.update = function(arr) {
    return apiService.call('PUT', 'user/update', arr);
  };

  this.updateAvailUser = function(obj) {
    return apiService.call('PUT', 'user/update/available', obj);
  };

  this.addAvailUser = function(status, user) {
    return apiService.call('GET', `user/available-user/${status}/${user}`);
  };

  this.removeAvailUsers = function(status, arr) {
    console.log('update', arr);
    let defer = $q.defer();
    if (arr && arr.length > 0) {
      apiService.call('PUT', `user/available-user/${status}`, arr)
        .then(res => {
          defer.resolve(res);
        });
    }

    return defer.promise;
  };

  this.updateSocial = function(obj) {
    return apiService.call('PUT', 'user/update/social', obj);
  };

  this.removeSocial = function(arr) {
    return apiService.call('POST', 'user/update/social', arr);
  };

  return this;
}

module.exports = angular
  .module('common')
  .service('userService', userService)
  .name;