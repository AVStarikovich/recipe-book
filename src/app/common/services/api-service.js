'use strict';

const appConfig = require('../../config');

function apiService (
  $q,
  $http
) {

  this._accessToken = '';

  this.setToken = function(token) {
    this._accessToken = token;
  };

  this.call = function(method, url, data) {
    let defer = $q.defer(),
        prepare = {
          method,
          url: `${appConfig.api}/${url}`
        };
    if (data) {
      prepare.data = data;
    }
    if (this._accessToken) {
      prepare.headers = {
        'token': this._accessToken
      };
    }
    $http(prepare)
      .then(res => {
        if (res.data.success) {
          defer.resolve(res.data.data);
        } else defer.reject(res.data);
      })
      .catch(rej => {
        defer.reject(rej);
      });

    return defer.promise;
  };

  this.removeToken = function() {
    this._accessToken = '';
  }

  return this;
}

module.exports = angular
  .module('common')
  .service('apiService', apiService)
  .name;