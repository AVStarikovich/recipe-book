'use strict'

function vocService(
  apiService
) {
  
  this.get = function(str) {
    return apiService.call('GET', `vocabulary/${str}`);
  };

  return this;
}

module.exports = angular
  .module('common')
  .service('vocService', vocService)
  .name;