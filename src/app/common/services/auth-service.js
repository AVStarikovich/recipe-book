'use strict'

function authService(
  userService,
  apiService,
  $state,
  $q,
  $rootScope
) {

  console.log('authService');

  this._user = null;
  this._accessToken = '';

  this.getUser = function() {
    return this._user;
  };

  this.getToken = function() {
    return this._accessToken;
  };

  this.setToken = function(token) {
    this._accessToken = token;
    return this;
  };

  this.setUser = function(obj) {
    this._user = obj;
    this._accessToken = obj.accessToken;
    localStorage.setItem('accessToken', this._accessToken);
    $rootScope.$broadcast('setUser', obj);
    return this._user;
  };

  this.login = function(login, password) {
    return userService.login(login, password)
      .then(user => {
        return this.setUser(user);
      });
  };

  this.logOut = function() {
    localStorage.removeItem('accessToken');
    this._user = null;
    this._accessToken = '';
    apiService.removeToken();
  };

  this.loginByToken = function(token) {
    return userService.loginByToken(token)
      .then(user => {
        return this.setUser(user);
      })
  };

  this.reLog = function() {
    let defer = $q.defer(),
        accessToken = localStorage.getItem('accessToken');
        
    if (this._accessToken && this._accessToken.length) {
      console.log('loginByToken, _accessToken', this._accessToken);
      this.loginByToken(this._accessToken)
        .then(user => {
          this.setUser(user);
          defer.resolve(user);
        })
        .catch(rej => {
          this.setToken('');
          defer.reject(rej);
        })
    } else if (accessToken && accessToken.length) {
        console.log('loginByToken, accessToken', accessToken);
        this.loginByToken(accessToken)
          .then(user => {
            this.setUser(user);
            defer.resolve(user);
          }) 
          .catch(rej => {
            this.setToken('');
            defer.reject(rej);
          })
      } else defer.reject(false);
    return defer.promise;
  }

  this.signIn = function(login, password, email) {
    return userService.signIn(login, password, email);
  };

  this.updateUser = function(arr) {
    return userService.updateUser(arr);
  };

  return this;
}


module.exports = angular
  .module('common')
  .service('authService', authService)
  .name;