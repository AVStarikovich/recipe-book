'use strict'

const config = require('../../config');
const modalSignIn = require('../../main/signin/signin-ctrl');
const modalLogIn = require('../../main/login/login-ctrl');


function modalService(
  $uibModal
) {

  this.open = function(ctrl, url) {
     let open = $uibModal.open({
      templateUrl: `${config.tmpUrl}${url}`,
      controller: ctrl,
      controllerAs: '$mdl',
      animate: true
    })
  };

  this.close = function($umi) {
    $umi.dismiss('cancel');
  };

  console.log('modalService');

}

module.exports = angular
  .module('common')
  .service('modalService', modalService)
  .name;