'use strict'

function catService(
  apiService
  ) {

  this.get = function() {
    return apiService.call('GET', 'category/list');
  }

  this.getById = function(id) {
    return apiService.call('GET', `category/${id}`);
  }

  this.getByIds = function(arr) {
    return apiService.call('POST', 'category', arr);
  }

  return this;
}

module.exports = angular
  .module('common')
  .service('catService', catService)
  .name;