'use strict'

function ingService(
  $q,
  apiService
  ) {

  this.arch = {}; 

  this.get = function() {
    return apiService.call('GET', 'ingredient/list');
  }

  this.getById = function(id) {
    let defer = $q.defer();

    if (this.arch[id] == undefined){
      apiService.call('GET', `ingredient/${id}`)
        .then( res => {
          this.arch[res.id] = res
          defer.resolve(res);
        })
        .catch( rej => {
          console.log(rej);
          defer.reject(rej);
        });
    } else defer.resolve(this.arch[id]);
    return defer.promise;
  };

  this.getByIds = function(data) {
    let arr = [],
        result = [],
        defer = $q.defer();

        console.log('objectkeys', Object.keys(this.arch));

    for (var i = 0; i < data.length; i++) {
      if (Object.keys(this.arch).indexOf(_.toString(data[i])) == -1){
        arr.push(data[i]);
      } else result.push(this.arch[data[i]]);
    }
    if (arr.length) {
      apiService.call('POST', 'ingredient', arr)
        .then( res => {
          for (var i =0; i < res.length; i++) {
            this.arch[res[i].id] = res[i];
          }
          defer.resolve(result.concat(res)); 
        })
        .catch ( rej => {
          console.log(rej);
          defer.reject(rej);
        });
    } else defer.resolve(result);
    return defer.promise;
  };

  return this;
}

module.exports = angular
  .module('common')
  .service('ingService', ingService)
  .name;