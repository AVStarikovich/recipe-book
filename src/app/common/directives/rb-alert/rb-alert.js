'use strict';

const appConfig = require('./../../../config');

function rbAlert(

) {
  return {
    restrict: 'E',
    templateUrl: `${appConfig.tmpUrl}/common/directives/rb-alert/rb-alert-view.html`,
    link: function(scope) {
      scope.alerts = [];
      scope.$on('openAlert', (event, data) => {
        scope.alerts.push({
          dismiss: data.dismiss || 500000,
          type: data.type,
          msg: data.msg
        });
      });

      scope.closeAlert = function(index) {
        scope.alerts.splice(index, 1);
      };
    }
  }
}

module.exports = angular
  .module('common')
  .directive('rbAlert', rbAlert)
  .name;