'use strict'

function userInfo(
  $timeout,
  userService
) {
  return {
    template: function() {return require('./user-info-view.pug')()},
    transclude: 'true',
    restrict: 'A',
    link: function (
      scope,
      element,
      attribute
    ) {
      scope.data = {
        user: null,
        hide: false
      };
      element.on('mouseenter', event => {
        event.stopPropagation();
        userService.getUserByLogin(attribute.userInfo)
          .then(user => {
            scope.data.hide = true;
            scope.data.user = _.cloneDeep(user);
            scope.data.user.lastLogin = moment(user.lastLogin).format('ll');
          });
      })

      element.on('mouseleave', event => {
        event.stopPropagation();
        $timeout(() => {
          scope.data.hide = false;
          scope.data.user = null;
        })
        console.log('user scope', scope.data.user);
      })
    }
  }

}

module.exports = angular
  .module('common')
  .directive('userInfo', userInfo)
  .name;