'use strict';

module.exports = angular
  .module('common', [])
  .name;

/**
 * Services
 */
require('./services/api-service');
require('./services/user-service');
require('./services/auth-service');
require('./services/recipe-service');
require('./services/ing-service');
require('./services/cat-service');
require('./services/vocabulary-service');
require('./services/modal-service');

/**
 * Directives
 */
require('./directives/rb-alert/rb-alert');
require('./directives/user-info/user-info');

/**
 * Components
 */