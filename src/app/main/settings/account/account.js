'use strict'

const config = require('../../../config');

function settingsAccount(
  authService,
  vocService,
  $scope
) {

  $scope.$parent.vm.catOf = 'account';

  let $ctrl = this;
  
  (function init(){

    $ctrl.user = _.cloneDeep(authService.getUser())

    console.log($ctrl.user, 'account user');
  })();

}

module.exports = angular
  .module('main')
  .component('settingsAccount', {
    templateUrl: `${config.tmpUrl}/main/settings/account/account.html`,
    controller: settingsAccount,
    controllerAs: '$ctrl'
  })
  .name;