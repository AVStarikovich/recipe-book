'use strict'

const config = require('../../../config');

function settingsGeneral(
  authService,
  userService,
  vocService,
  $scope
) {

  $scope.$parent.vm.catOf = 'general';

  let user = _.cloneDeep(authService.getUser()),
      $ctrl = this;
  
  console.log($ctrl);
  
  (function init(){

    $ctrl.user = _.cloneDeep(user);

  })();

  $ctrl.saveChanges = function(){
    console.log('savechanges');
    if ($ctrl.whatIsChanged().length > 0) {
      userService.update($ctrl.whatIsChanged())
        .then(data => {
          user = data;
          $scope.$emit('openAlert', {
            msg: 'saved',
            type: 'seccess'
          });
        })
        .catch(rej => {
          $scope.$emit('openAlert', {
            msg: rej.error.message,
            type: 'danger'
          });
        })
    }
  };

  $ctrl.whatIsChanged = function() {
    let result = [];

    if ($ctrl.user.name != user.name) {
      result.push({
        fieldName : 'name',
        fieldValue : $ctrl.user.name
      });
    }
    if ($ctrl.user.lastname != user.lastname) {
      result.push({
        fieldName : 'lastname',
        fieldValue : $ctrl.user.lastname
      });
    }
    if ($ctrl.user.gender != user.gender) {
      result.push({
        fieldName : 'gender',
        fieldValue : $ctrl.user.gender
      });
    }
    if ($ctrl.user.dateBirth != user.dateBirth) {
      result.push({
        fieldName : 'dateBirth',
        fieldValue : $ctrl.user.dateBirth
      });
    }

    console.log(result, 'result');

    return result;
  };

  vocService.get('gender')
    .then(data => {
      $ctrl.gender = data;
    });

}

module.exports = angular
  .module('main')
  .component('settingsGeneral', {
    templateUrl: `${config.tmpUrl}/main/settings/general/general.html`,
    controller: settingsGeneral,
    controllerAs: '$ctrl'
  })
  .name;