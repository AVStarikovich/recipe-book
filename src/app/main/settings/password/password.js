'use strict'

const config = require('../../../config');

function settingsPassword(
  authService,
  vocService,
  $scope
) {

  $scope.$parent.vm.catOf = 'password';

  let $ctrl = this;
  
  (function init(){

    $ctrl.password = '';
    $ctrl.newPassword = '';
    $ctrl.confirmedPassword = '';

    console.log('password ctrl  ');
  })();

}

module.exports = angular
  .module('main')
  .component('settingsPassword', {
    templateUrl: `${config.tmpUrl}/main/settings/password/password.html`,
    controller: settingsPassword,
    controllerAs: '$ctrl'
  })
  .name;