'use strict'

const config = require('../../../config');

function settingsSocial(
  authService,
  vocService,
  $scope
) {

  $scope.$parent.vm.catOf = 'social';

  let $ctrl = this,
      user = _.cloneDeep(authService.getUser());
 
  (function init(){

    $ctrl.user = _.cloneDeep(user);
    
    console.log($ctrl.user, 'social user');
  })();

  
  $ctrl.whatIsChanged = function() {
    let result = {};

    _.each(user.social, (value, key) => {
      if ($ctrl.user.social[key] != user.social[key]) {
        result[key] = value;
      }
    })
    
  }

  vocService.get('social')
    .then(data => {
      console.log(data, 'voc');
      $ctrl.social = data.items;
    });
}

module.exports = angular
  .module('main')
  .component('settingsSocial', {
    templateUrl: `${config.tmpUrl}/main/settings/social/social.html`,
    controller: settingsSocial,
    controllerAs: '$ctrl'
  })
  .name;