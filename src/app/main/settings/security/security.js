'use strict'

const config = require('../../../config');

function settingsSecurity(
  authService,
  userService,
  vocService,
  $scope
) {

  $scope.$parent.vm.catOf = 'security';
  
  let $ctrl = this,
      user = _.cloneDeep(authService.getUser());
  
  (function init(){

    $ctrl.rmAccessUsers = [];
    $ctrl.rmDeniedUsers = [];

    $ctrl.user = _.cloneDeep(user);

    console.log($ctrl.user, 'security user');
  })();

  $ctrl.saveChanges = function() {

    if ($ctrl.rmAccessUsers.length > 0) {
      $ctrl.updateUsers('access');
    }
    if ($ctrl.rmDeniedUsers.length > 0) {
      $ctrl.updateUsers('denied');
    }

    if ($ctrl.user.available.recipe != user.available.recipe || $ctrl.user.available.profile != user.available.profile) {
      userService.updateAvailUser({
        recipe : _.toInteger($ctrl.user.available.recipe) ,
        profile : _.toInteger($ctrl.user.available.profile)
      }).then(data => {
        $ctrl.user = ata;
      })
      .catch(rej => {
        $scope.$emit('openAlert', {
          msg: rej.error.message,
          type: 'danger'
        });
      });
    }
      
  };

  $ctrl.updateUsers = function(type) {
    let rm = '';
    if (type == 'access') {
      rm = 'rmAccessUsers'
    }
    if (type == 'denied') {
      rm = 'rmDeniedUsers'
    }
    userService.removeAvailUsers(type, $ctrl[rm])
      .then(data => {
        $ctrl.user = data;
        $ctrl[rm] = [];
      })
      .catch(rej => {
        $scope.$emit('openAlert', {
          msg: rej.error.message,
          type: 'danger'
        });
      });
  };

  vocService.get('access_level')
    .then(data => {
      $ctrl.accessLevel = data;
    });

  $ctrl.deniedUser = function (data) {
    if ($ctrl.rmDeniedUsers.indexOf(data) >= 0) {
      delete $ctrl.rmDeniedUsers[$ctrl.rmDeniedUsers.indexOf(data)]
    } else $ctrl.rmDeniedUsers.push(data);
    console.log('deniedUser', $ctrl.rmDeniedUsers);
  };

  $ctrl.accessUser = function (data) {
    if ($ctrl.rmAccessUsers.indexOf(data) >= 0) {
      delete $ctrl.rmAccessUsers[$ctrl.rmAccessUsers.indexOf(data)]
    } else $ctrl.rmAccessUsers.push(data);
  };

}

module.exports = angular
  .module('main')
  .component('settingsSecurity', {
    template: function(){return require('./security.pug')();},
    controller: settingsSecurity,
    controllerAs: '$ctrl'
  })
  .name;