'use strict';

function initCtrl (
  apiService,
  $uibModal
) {
  var vm = this;

  console.log('initCtrl');
}

module.exports = angular
  .module('main')
  .controller('initCtrl', initCtrl)
  .name;