'use strict'

function profileCtrl(
  $state,
  $scope,
  authService,
  vocService,
  userService,
  user
  ) {
  
  let vm = this;

  vm.regExp = '\\w+';
  vm.editMod = false;
  vm.collapsedClass = 'fa-angle-down'
  vm.isCollapsedSocial = true;
  vm.changeProfileImgClass = '';
  vm.rmAccessUsers = [];
  vm.rmDeniedUsers = [];
  vm.test = 'test';

  (function init() {

    vm.user = _.cloneDeep(user);
    vm.state = $state;
    vm.user.registered = moment(vm.user.registered).fromNow();
    vm.authorImgAdr= 'https://pp.vk.me/c626426/v626426312/25b03/zoxip8adOZo.jpg';

  })();

  vm.collapseSocial = function() {
    if (vm.isCollapsedSocial) {
      vm.collapsedClass = 'fa-angle-up';
    } else vm.collapsedClass = 'fa-angle-down';
    vm.isCollapsedSocial = !vm.isCollapsedSocial  
  };

  vm.changeEditMod = function () {
    vm.user = _.cloneDeep(user);
    vm.editMod = !vm.editMod;
  };

  vm.saveChanges = function() {
   
    let saveCatch = false;

    userService.removeAvailUsers('access', vm.rmAccessUsers)
      .then(data => {
        user = data;
      })
      .catch(rej => {
        saveCatch = true;
        $scope.$emit('openAlert', {
          msg: rej.error.message,
          type: 'danger'
        });
      });
    
    userService.removeAvailUsers('denied', vm.rmDeniedUsers)
      .then(data => {
        user = data;
      })
      .catch(rej => {
        saveCatch = true;
        $scope.$emit('openAlert', {
          msg: rej.error.message,
          type: 'danger'
        });
      });
   
    if (vm.user.available.recipe != user.available.recipe || vm.user.available.profile != user.available.profile) {
      userService.updateAvailUser({
        recipe : _.toInteger(vm.user.available.recipe) ,
        profile : _.toInteger(vm.user.available.profile)
      }).then(data => {
        user = data;
      })
      .catch(rej => {
        saveCatch = true;
        $scope.$emit('openAlert', {
          msg: rej.error.message,
          type: 'danger'
        });
      });
    }
    
    if (vm.whatIsChanged().length > 0) {
      userService.update(vm.whatIsChanged())
        .then(data => {
          user = data;
        })
        .catch(rej => {
          saveCatch = true;
          $scope.$emit('openAlert', {
            msg: rej.error.message,
            type: 'danger'
          });
        })
    }

    if (!saveCatch) {
      vm.editMod = !vm.editMod;
    } saveCatch = false;
  };

  vm.whatIsChanged = function() {
    let result = [];

    for(let key in user) {
      switch (key) {
        case 'registered': break
        case 'available': break
        default: 
          if (user[key] != vm.user[key]) {
            result.push({
              fieldName : key,
              fieldValue : vm.user[key]
            });
          }
      } 
    }

    return result;
  };

  vm.deniedUser = function (data) {
    if (vm.rmDeniedUsers.indexOf(data) >= 0) {
      delete vm.rmDeniedUsers[vm.rmDeniedUsers.indexOf(data)]
    } else vm.rmDeniedUsers.push(data);
  };

  vm.accessUser = function (data) {
    if (vm.rmAccessUsers.indexOf(data) >= 0) {
      delete vm.rmAccessUsers[vm.rmAccessUsers.indexOf(data)]
    } else vm.rmAccessUsers.push(data);
  };

  vm.addUser11 = function(user) {
    userService.addAvailUser('denied', user)
      .then(data => {
        vm.user.available.deniedUser = data.available.deniedUser;
      });
  };

  vm.removeDeniedUser11 = function(user) {
    let go = [];
    go.push(user);
    console.log(go);
    userService.removeAvailUsers('denied', go)
      .then(data => {
        vm.user.available.deniedUser = data.available.deniedUser;
      });
  };


}

module.exports = angular
  .module('main')
  .controller('profileCtrl', profileCtrl)
  .name;