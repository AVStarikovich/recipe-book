'use strict'

const config = require('../../config');
const modalLogIn = require('../login/login-ctrl');
const modalSignIn = require('../signin/signin-ctrl');

function headerCtrl(
  modalService,
  authService,
  $scope,
  $rootScope,
  $state
) {
  let $header = this;
  
  $header.user = null;

  $header.user = authService.getUser();
  console.log('as', $header.user);

  $scope.$on('setUser', (event, data) => {
    $header.user = data;
    console.log('on', $header.user);
  })

  $header.modalLogInOpen = function() {
    modalService.open(modalLogIn, '/main/login/login-view.html');
  };

  $header.modalSignInOpen = function() {
    modalService.open(modalSignIn, '/main/signin/signin-view.html');
  };

  $header.logOut = function() {
    authService.logOut();
    $header.log = false;
    $header.isAdmin = false;
    $state.go('home', {}, {reload: true});
  };

  console.log('headerCtrl');
}

module.exports = angular
  .module('main')
  .controller('headerCtrl', headerCtrl)
  .name;