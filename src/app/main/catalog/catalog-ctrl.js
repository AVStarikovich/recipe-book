'use strict';

function catalogCtrl (
  $state,
  $scope,
  recipeService,
  ingService,
  catService
) {
  let vm = this,
      ingIds = [];

  (function init() {
    vm.sortOf = 'rating';
    vm.catOf = '';

    recipeService.get()
      .then(res => {
        console.log('recipes ',res);
        vm.recipes = res;
        for (var i = 0; i < vm.recipes.length; i++) {
          ingIds = ingIds.concat(vm.recipes[i].ingredients);
        }
        ingIds = _.uniq(ingIds);
        ingService.getByIds(ingIds)
          .then(res => {
            console.log('ings', res);
            vm.ings = res;
          });
      })

    catService.get()
      .then(res => {
        console.log('cats', res);
        vm.cats = res;
      })

    console.log('catalogCtrl');

  })();

}

module.exports = angular
  .module('main')
  .controller('catalogCtrl', catalogCtrl)
  .name;