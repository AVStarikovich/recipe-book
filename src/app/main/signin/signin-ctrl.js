'use strict'

function modalSignIn(
  modalService,
  authService,
  $uibModalInstance
) {
  let vm = this;
  vm.login = '';
  vm.password = '';
  vm.email = '';
  vm.regExp = '\\w+';
  vm.errMessage = '';
  
  console.log('modal sign in');

  vm.ok = function() {
    authService.signIn(vm.login, vm.password, vm.email)
    .then(data => {
      if (data.id) {
        modalService.close($uibModalInstance);
      }
    }).catch(data => {
      if (data.error.code == 11000) {
        vm.errMessage = `${data.error.message} is busy`
      }
    })
  };

  vm.close = function() {
    modalService.close($uibModalInstance);
  };

}

module.exports = modalSignIn;