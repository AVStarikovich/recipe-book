'use strict';

const appConfig = require('../config');

function config ($stateProvider) {
  $stateProvider
  .state('init', {
    url: '/',
    views: {
      '': {
        templateUrl: `${appConfig.tmpUrl}/main/init/init-view.html`
      },
      'main@init': {
        template: `
          <ui-view></ui-view>
        `,
        controller: function(

        ) {
          console.log('main ctrl');
        },
        controllerAs: '$main'
      },
      'header@init': {
        templateUrl: `${appConfig.tmpUrl}/main/header/header-view.html`,
        controller: 'headerCtrl', 
        controllerAs: '$header'
      },
      'footer@init': {
        templateUrl: `${appConfig.tmpUrl}/main/footer/footer-view.html`,
        controller: 'footerCtrl', 
        controllerAs: '$footer'
      }
    }
  })
  .state('home', {
    parent: 'init',
    url: 'home',
    templateUrl: `${appConfig.tmpUrl}/main/home/home-view.html`
  })
  .state('catalog', {
    parent: 'init',
    url: 'catalog',
    templateUrl: `${appConfig.tmpUrl}/main/catalog/catalog-view.html`,
    controller: 'catalogCtrl',
    controllerAs: 'vm'
  })
  .state('recipe', {
    parent: 'init',
    url: 'recipe/:recipeId',
    templateUrl: `${appConfig.tmpUrl}/main/recipe/recipe-view.html`,
    controller: 'recipeCtrl',
    controllerAs: 'vm'
  })
  .state('getUser', {
    parent: 'init',
    abstract: true,
    template: '<ui-view></ui-view>',
    resolve: {
      user: function(
        $q,
        $rootScope,
        authService
      ) {
        let defer = $q.defer(),
            user = authService.getUser();

        if (user) {
          defer.resolve(user);
        } else {
          $rootScope.$on('setUser', (event, data) => {
            defer.resolve(data);
          })
        }

        return defer.promise;
      }
    }
  })
  .state('myProfile', {
    parent: 'getUser',
    url: 'profile',
    templateUrl: `${appConfig.tmpUrl}/main/profile/profile-view.html`,
    controller: 'profileCtrl',
    controllerAs: 'vm',
    data: {
      access: 'loggedIn'
    }
  })
  .state('userProfile', {
    parent: 'init',
    url: 'profile/:userLogin',
    templateUrl: `${appConfig.tmpUrl}/main/profile/profile-view.html`,
    controller: 'profileCtrl',
    controllerAs: 'vm',
    resolve: {
      user: function(
        $q,
        userService,
        $stateParams
      ) {
        return userService.getUserByLogin($stateParams.userLogin);
      }
    }
  })
  .state('settings', {
    parent: 'getUser',
    abstract: true,
    url: 'settings',
    templateUrl: `${appConfig.tmpUrl}/main/settings/settings-view.html`,
    controller: 'settingsCtrl',
    controllerAs: 'vm'
  })
  .state('account', {
    parent: 'settings',
    url: '/account',
    template: '<settings-account></settings-account>'
  })
  .state('password', {
    parent: 'settings',
    url: '/password',
    template: '<settings-password></settings-password>'
  })
  .state('general', {
    parent: 'settings',
    url: '/general',
    template: '<settings-general></settings-general>'
  })
  .state('social', {
    parent: 'settings',
    url: '/social',
    template: '<settings-social></settings-social>'
  })
  .state('security', {
    parent: 'settings',
    url: '/security',
    template: '<settings-security></settings-security>'
  })
  // .state('main', {
    
  // })
  // .state('home', {
  //   parent: 'init',
  //   url: '/',
  //   templateUrl: `${appConfig.tmpUrl}/main/home/home-view.html`,
  //   controller: function() {
  //     console.log('test');
  //   }
  // })
  // .state('catalog', {
  //   parent: 'init',
  //   url: '/catalog',
  //   templateUrl: `${appConfig.tmpUrl}/main/catalog/catalog-view.html`,
  //   controller: 'catalogCtrl',
  //   controllerAs: 'vm'
  // })
  // .state('recipe', {
  //   parent: 'init',
  //   url: '/recipe/:recipeId',
  //   templateUrl: `${appConfig.tmpUrl}/main/recipe/recipe-view.html`,
  //   controller: 'recipeCtrl',
  //   controllerAs: 'vm'
  // })
  // .state('profile', {
  //   parent: 'init',
  //   template: '<ui-view></ui-view>',
  //   url: '/profile',
  //   controller: function() {
  //     console.log('abstract profile');
  //   },
  //   abstract: true,
  //   resolve: {
  //     user: function(
  //       $q,
  //       $rootScope,
  //       authService
  //     ) {
  //       let defer = $q.defer()
  //         , user = authService.getUser();

  //       if (user) {
  //         defer.resolve(user);
  //       } else {
  //         $rootScope.$on('setUser', (event, data) => {
  //           defer.resolve(data);
  //         })
  //       }

  //       return defer.promise;
  //     }
  //   }
  // })
  // .state('myProfile', {
  //   parent: 'profile',
  //   url: '',
  //   templateUrl: `${appConfig.tmpUrl}/main/profile/profile-view.html`,
  //   controller: 'profileCtrl',
  //   controllerAs: 'vm',
  //   data: {
  //     access: 'loggedIn'
  //   }
  // })
  // .state('usersProfile', {
  //   parent: 'profile',
  //   url: '/:userLogin',
  //   controller: function() {
  //     console.log('myProfile');
  //   },
  //   resolve: {
  //     user: function() {}
  //   }
  // })

}

module.exports = config;