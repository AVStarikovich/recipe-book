'use strict'

function modalLogIn(
  modalService,
  authService,
  $uibModalInstance
) {
  let vm = this;
  vm.login = '';
  vm.password = '';
  vm.loginErr = '';

  vm.ok = function() {
    authService.login(vm.login, vm.password)
      .then(res => {
        modalService.close($uibModalInstance);
      })
      .catch(data => {
        console.log(data);
        vm.loginErr = data.message;
      })

  };

  vm.close = function() {
    modalService.close($uibModalInstance);
  };

}

module.exports = modalLogIn;