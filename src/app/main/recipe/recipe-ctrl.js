'use strict';

function recipeCtrl (
  $scope,
  $state,
  recipeService
) {
  let vm= this;

  (function init() {
    console.log('recipeCtrl');
  
    recipeService.getById($state.params.recipeId)
      .then(res => {
        vm.recipe = res;
        console.log(vm.recipe);
      });
  
    vm.recipeImgAdr= 'http://netolkoeda.com/wp-content/uploads/2014/05/appp.jpg';
    vm.authorImgAdr= 'https://pp.vk.me/c626426/v626426312/25b03/zoxip8adOZo.jpg';

    vm.rate = 2;

    vm.ratingStates = [
      {stateOn: 'glyphicon-star', stateOff: 'glyphicon-star-empty'},
      {stateOn: 'glyphicon-star', stateOff: 'glyphicon-star-empty'},
      {stateOn: 'glyphicon-star', stateOff: 'glyphicon-star-empty'},
      {stateOn: 'glyphicon-star', stateOff: 'glyphicon-star-empty'},
      {stateOn: 'glyphicon-star', stateOff: 'glyphicon-star-empty'},
    ];

  })();

}

module.exports = angular
  .module('main')
  .controller('recipeCtrl', recipeCtrl)
  .name;