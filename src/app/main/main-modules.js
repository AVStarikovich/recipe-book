'use strict';

const config = require('./main-routes');

module.exports = angular.module('main', [])
  .config(config)
  .name;

require('./init/init-ctrl');
require('./catalog/catalog-ctrl');
require('./recipe/recipe-ctrl');
require('./header/header-ctrl');
require('./profile/profile-ctrl');
require('./footer/footer-ctrl');
require('./settings/settings-ctrl');

/**
 * Components
 */

require('./settings/account/account.js');
require('./settings/password/password.js');
require('./settings/general/general.js');
require('./settings/social/social.js');
require('./settings/security/security.js');