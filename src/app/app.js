'use strict';

window._ = require('lodash');
window.moment = require('moment');

const angular = require('angular');
const uiRouter = require('angular-ui-router');
const ngBootstrap = require('angular-ui-bootstrap');

const config = require('./routes');
const main = require('./main/main-modules');
const common = require('./common/common-modules');


angular
  .module('app', [
    uiRouter,
    ngBootstrap,
    main,
    common
  ])
  .config(config)
  .run(($rootScope, $state, authService) => {
    $rootScope.$on('$stateChangeStart', (
      event,
      toState,
      toParams,
      fromState,
      fromParams,
      options
    ) => {
      console.log('run');

      console.log('ToState', toState);
      console.log('from ', fromState);

      if (authService.getUser()) return;
      authService.reLog()
        .then(user => {
          console.log('user', user);
          if (toState.data && toState.data.access != 'loggedIn') {
            event.preventDefault();
            $state.go('home');
          } 
          return user;
        })
        .catch(rej => {
          console.log('rej run');
          console.log(rej);
          if (toState.name !== 'home') {
            event.preventDefault();
            $state.go('home');
          }
        })
    })
  });