'use strict';

function config (
  $stateProvider,
  $urlRouterProvider,
  $locationProvider
) {
  $urlRouterProvider.otherwise('/');
  $locationProvider.html5Mode(true);
}

module.exports = config;